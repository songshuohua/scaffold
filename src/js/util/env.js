/**
 * Created by songshuohua on 2017/1/12.
 */

/**
 * 判断环境 浏览器版本，运行平台
 * @class src.js.util.env
 * */
let Browser;
(function (Browser) {
    Browser[Browser["MSIE"] = 0] = "MSIE";
    Browser[Browser["FF"] = 1] = "FF";
    Browser[Browser["SAFARI"] = 2] = "SAFARI";
    Browser[Browser["CHROME"] = 3] = "CHROME";
    Browser[Browser["OPERA"] = 4] = "OPERA";
    Browser[Browser["OTHER"] = 5] = "OTHER";
})(Browser || (Browser = {}));

let Os;
(function (Os) {
    Os[Os["WINDOWS"] = 0] = "WINDOWS";
    Os[Os["IOS"] = 1] = "IOS";
    Os[Os["OSX"] = 2] = "OSX";
    Os[Os["ANDROID"] = 3] = "ANDROID";
    Os[Os["OTHER"] = 4] = "OTHER";
})(Os || (Os = {}));

let IOSType;
(function () {
    IOSType[IOSType["IPHONE"] = 0] = "IPHONE";
    IOSType[IOSType["IPAD"] = 1] = "IPAD";
    IOSType[IOSType["IPOD"] = 2] = "IPOD";
    IOSType[IOSType["OTHER"] = 3] = "OTHER";
})(IOSType || (IOSType = {}));

let Env = (function () {
    function Env() {
    }

    /**
     * InternetExplorer 返回true<br />
     * 判断浏览器是否为IE和 IE版本是否相对应
     *
     * @static
     * @method isIE
     * @param {number} version  如果没有指定ie版本，则判断是不是ie浏览器
     * @return {boolean}
     */
    Env.isIE = function (version) {
        if (typeof version === "undefined") {
            version = null;
        }
        if (version === null) {
            return Env.BROWSER === 0 /* MSIE */;
        }
        return Env.BROWSER === 0 /* MSIE */ && Env.VERSION === version;
    };

    /**
     * FireFox浏览器 true
     *
     * @static
     * @method isFF
     * @return {boolean}
     */
    Env.isFF = function () {
        return Env.BROWSER === 1 /* FF */;
    };

    /**
     * Safari浏览器 true
     *
     * @static
     * @method isSafari
     * @return {boolean}
     */
    Env.isSafari = function () {
        return Env.BROWSER === 2 /* SAFARI */;
    };

    /**
     * Chrome浏览器 true
     *
     * @static
     * @method isChrome
     * @return {boolean}
     */
    Env.isChrome = function () {
        return Env.BROWSER === 3 /* CHROME */;
    };

    /**
     * Windows系统 true
     *
     * @static
     * @method isWindows
     * @return {boolean}
     */
    Env.isWindows = function () {
        return Env.OS === 0 /* WINDOWS */;
    };

    /**
     * iOS系统 true
     *
     * @static
     * @method isIOS
     * @return {boolean}
     */
    Env.isIOS = function () {
        return Env.OS === 1 /* IOS */;
    };


    /**
     * OsX系统 true
     *
     * @static
     * @method isOsX
     * @return {boolean}
     */
    Env.isOsX = function () {
        return Env.OS === 2 /* OSX */;
    };

    /**
     * Android系统 true
     *
     * @static
     * @method isAndroid
     * @return {boolean}
     */
    Env.isAndroid = function () {
        return Env.OS === 3 /* ANDROID */;
    };

    /**
     * IPhone true
     *
     * @static
     * @method isIPhone
     * @return {boolean}
     */
    Env.isIPhone = function(){
        return Env.IOSType === 0 /* IPHONE */;
    };

    /**
     * IPad true
     *
     * @static
     * @method isIPad
     * @return {boolean}
     */
    Env.isIPad = function(){
        return Env.IOSType === 1 /* IPAD */;
    };

    /**
     * IPod true
     *
     * @static
     * @method isIPod
     * @return {boolean}
     */
    Env.isIPod = function(){
        return Env.IOSType === 2 /* IPOD */;
    };

    /**
     * 获取IE浏览器版本号
     * @static
     * @method getMSIEVersion
     * @return {Integer}
     */
    Env.getMSIEVersion = function () {
        if (Env.VERSION === undefined) {
            let matches = navigator.userAgent.match(/((msie|MSIE)\s|rv:)([\d\.]+)/);
            let version = null;
            if(matches != null){
                version = matches.length > 3 ? matches[3] : null;
            }
            if (version) {
                Env.VERSION = parseInt(version, 10);
            }
        }
        return Env.VERSION;
    };

    /**
     * 获取谷歌浏览器版本号
     * @static
     * @method getChromeVersion
     * @return {Integer}
     */
    Env.getChromeVersion = function() {
        if (Env.VERSION === undefined) {
            let matches = navigator.userAgent.toLowerCase().match(/chrome\/([\d.]+)/);
            let version = matches.length > 1 ? matches[1] : null;
            if (version) {
                Env.VERSION = parseInt(version, 10);
            }
        }
        return Env.VERSION;
    };

    /**
     * 获取火狐浏览器版本号
     * @static
     * @method getFirefoxVersion
     * @return {Integer}
     */
    Env.getFirefoxVersion = function() {
        if (Env.VERSION === undefined) {
            let matches = navigator.userAgent.toLowerCase().match(/firefox\/([\d.]+)/);
            let version = null;
            if(matches != null){
                version = matches.length > 1 ? matches[1] : null;
            }
            if (version) {
                Env.VERSION = parseInt(version, 10);
            }
        }
        return Env.VERSION;
    };

    /**
     * IE版本号
     */
    Env.IE6 = 6;
    Env.IE7 = 7;
    Env.IE8 = 8;
    Env.IE9 = 9;
    Env.IE10 = 10;
    Env.IE11 = 11;
    Env.IE12 = 12;

    return Env;
})();


module.exports = Env;
