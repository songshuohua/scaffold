/**
 * Created by songshuohua on 2017/1/12.
 */
import Tool from "./tool";

/**
 * 基础组件
 * @class src.js.util.component.js
 */
let Component = (function () {

    /**
     * @class component.Component
     * @constructor
     */
    function Component(options) {

        /**
         * 组件唯一ID
         * @property cid
         * @type {String}
         */
        this.cid = _.uniqueId('comp');
        options || (options = {});
        _.extend(this, options);

        this._init();
        this._ensureElement();
        this.initialize.apply(this, arguments);
    }


    /**
     * 是否已经渲染
     * @property rendered
     * @type {Boolean}
     * @default false
     */
    Component.prototype.rendered = false;

    /**
     * 延迟渲染
     * @property deferRender
     * @type {Boolean}
     * @default false
     */
    Component.prototype.deferRender = false;


    /**
     * Component 初始化方法
     * @method initialize
     * @param {void}
     * @return {void}
     * */
    Component.prototype.initialize = function () {

    };

    /**
     *
     * @method template
     * @public
     */
    /* eslint-disable no-unused-vars */
    Component.prototype.template = function (data) {
    };
    /* eslint-enable no-unused-vars */


    /**
     * 界面渲染后执行方法，渲染。
     * @method render
     * @public
     */
    Component.prototype.render = function (data, to) {
        let self = this;
        self.sel = to;
        if(!self.deferRender){
            if (typeof data === "undefined") {
                data = {};
            }
            if (typeof to === "undefined") {
                to = self.el;
            }
            self.setElement(to);
            //渲染后的html 放到指定的元素中去
            let _$el = self.$el;
            let html = self.template(data);
            if (!Tool.isEmpty(html)) {
                _$el.html(html);
            }

            self.rendered = true;
        }else{
            self.deferRender = false;
        }

        return this;
    };

    /**
     * 界面渲染后执行方法，可重写。
     * @method afterRender
     * @public
     */
    Component.prototype.afterRender = function () {
    };

    /**
     * 获取服务端数据。
     * @method fetch
     * @param options
     * @param post 是否使用POST {}
     * @example  options = {
     *   url: "",
     *   data: {},
     *   success:function(){},
     *   error:function(){}
     * }
     * @public
     */
    /* eslint-disable no-unused-vars */
    Component.prototype.fetch = function (options, post) {
    };
    /* eslint-enable no-unused-vars */

    /**
     * 设置组件Element。
     * @method setElement
     * @public
     */
    Component.prototype.setElement = function (element) {
        this._setElement(element);
        return this;
    };

    /**
     * 移除组件
     * @method remove
     * @public
     * */
    Component.prototype.remove = function () {
        this._removeElement();
        return this;
    };

    /**
     * 清空组件下的DOM元素
     * @method empty
     * @public
     * */
    Component.prototype.empty = function () {
        let self = this;
        if(!_.isUndefined(self.$el)){
            self.$el.empty();
        }
    };

    /**
     * 显示组件
     * @method show
     * @public
     * */
    Component.prototype.show = function () {
        let self = this;
        if(!_.isUndefined(self.$el)){
            self.$el.show();
        }
    };

    /**
     * 隐藏组件
     * @method hide
     * @public
     * */
    Component.prototype.hide = function () {
        let self = this;
        if(!_.isUndefined(self.$el)){
            self.$el.hide();
        }
    };

    /**
     * 初始基础参数  初始化render 方法
     * @method _init
     * @private
     * */
    Component.prototype._init = function (options) {
        let self = this;
        if (typeof options === "undefined") {
            options = {};
        }
        self.render = _.wrap(self.render, function (render) {
            let arg = [];
            for (let _i = 0; _i < arguments.length - 1; _i++) {
                arg[_i] = arguments[_i + 1];
            }
            self.beforeRender();
            render.call(self, arg[0], arg[1]);
            self.afterRender();
            return self;
        });
    };


    /**
     * 检测元素是否存在
     * @method _ensureElement
     * @private
     * */
    Component.prototype._ensureElement = function () {
        if (!this.el) {
            let attrs = _.extend({}, _.result(this, 'attributes'));
            if (this.id) attrs.id = _.result(this, 'id');
            if (this.className) attrs['class'] = _.result(this, 'className');
            this.setElement(this._createElement(_.result(this, 'tagName')));
            this._setAttributes(attrs);
        } else {
            this.setElement(_.result(this, 'el'));
        }
    };


    /**
     * 创建元素
     * @method _createElement
     * @private
     * */
    Component.prototype._createElement = function (tagName) {
        return document.createElement(tagName);
    };


    /**
     * 设置元素
     * @method _setElement
     * @private
     * */
    Component.prototype._setElement = function (el) {
        let self = this;
        self.$el = el instanceof $ ? el : $(el);
        if(Tool.isEmpty(self.$el.attr("data-id"))){
            self.$el.attr("data-id", self.cid);
        }
        self.el = self.$el[0];
    };

    /**
     * 设置元素基本属性
     * @method _setAttributes
     * @private
     * */
    Component.prototype._setAttributes = function (attributes) {
        this.$el.attr(attributes);
    };

    /**
     * 清空组件dom
     * @method _removeElement
     * @private
     * */
    Component.prototype._removeElement = function () {
        this.$el.empty();
    };


    /**
     *  Component 继承实现
     *  @method extend
     * */
    let extend = function (protoProps, staticProps) {
        let parent = this;
        let child;
        if (protoProps && _.has(protoProps, 'constructor')) {
            child = protoProps.constructor;
        } else {
            child = function () {
                return parent.apply(this, arguments);
            };
        }
        _.extend(child, parent, staticProps);
        let Surrogate = function () {
            this.constructor = child;
        };
        Surrogate.prototype = parent.prototype;
        child.prototype = new Surrogate;
        if (protoProps) _.extend(child.prototype, protoProps);
        child.__super__ = parent.prototype;
        return child;
    };

    Component.extend = extend;

    return Component;
})();

module.exports = Component;
