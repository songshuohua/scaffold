/**
 * Created by songshuohua on 2017/1/12.
 */
import Env from "./env";

/**
 * 工具类
 * @class src.js.util.tool
 */
let Tool = {};
/**
 * 单位
 */
Tool.unit_100 = 100;
/**
 * 显示小数点后几位
 * */
Tool.DECIMAL_PLACE = 2;

/**
 * 显示小数点后几位
 * */
Tool.DECIMAL_PLACE_3 = 3;

/**
 * 显示小数点后几位
 * */
Tool.DECIMAL_PLACE_0 = 0;

/**
 * 判断是否为空
 * @method isEmpty
 * @param  val
 * @return {boolean}
 */
Tool.isEmpty = function (val) {
    return val === null || val === undefined || val === "" || val.length === 0;
};

/**
 *
 * @method isZeroOrEmpty
 * @param  val
 * @return {boolean}
 */
Tool.isZeroOrEmpty = function (val) {
    return val === null || val === undefined || val === "" || val.length === 0 || parseFloat(val) == 0;
};
/**
 * 工具类 获取url 参数
 * @method getUrlParam
 * @private
 */
Tool.getUrlParam = function (name) {
    let url = decodeURI(location.search); //获取url中"?"符后的字串
    let theRequest = new Object();
    if (url.indexOf("?") != -1) {
        let str = url.substr(1);
        let strs = str.split("&");
        for (let i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest[name];
};

/**
 * 若大于或等于万为则转为几万
 * 例：9999  => 9999.00
 *    10000  => 1.00万
 *    12345  =>1.2345万
 */
Tool.addWangUnit = function (text) {
    if (isNaN(text)) {
        return text;
    }
    let num = Number(text).toFixed(2);
    if (num < 10000) {
        return num;
    }
    num = (Number(text) / 10000).toFixed(6);
    let strNum = num.toString();
    let len = strNum.length;
    for (let i = 1; i <= 4; i++) {
        if (strNum.charAt(len - i) == '0') {
            strNum = strNum.substring(0, strNum.length - 1);
        } else {
            break;
        }
    }
    return strNum + " 万";
};

/**
 * 获取当前日期
 * yyyyMMdd
 */
Tool.getTodayFormatDate = function () {
    let date = new Date();
    let seperator1 = "";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    return currentdate;
};

/**
 * 获取当前时分秒，补0
 * HHmmss
 */
Tool.getTodayFormatTime = function () {
    let date = new Date();

    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();

    if (hour < 10) {
        hour = "0" + hour;
    }

    if (minute < 10) {
        minute = "0" + minute;
    }

    if (second < 10) {
        second = "0" + second;
    }

    let currentTime = hour + "" + minute + "" + second;
    return currentTime;
};

/**
 * 获取当前时间毫秒数
 * yyMMddhhmm 或yyMdhm、yyMddhhmm ...不补0
 */
Tool.getNowTime = function () {
    return new Date().getTime();
};

/**
 * 四舍五入
 * @param number 需要处理的数字
 * @param digits 小数位数
 */
Tool.toFixed = function (number, digits) {
    if (number === "") {
        return "";
    }
    if (isNaN(number)) {
        return number;
    }
    let object = new Number(number);
    let result = object.toFixed(digits);

    if (result == "-0.00") { // 解决部分负数保留两位小数后，-0.00，0.00 页面样式不一致问题，如 -0.0000012
        result = "0.00";
    }

    return result;
};

/**
 * 时间格式化
 * new date() >> yyyy-MM-dd hh:mm:ss
 */
Tool.formatDateTime = function (strTime) {
    let date;
    if (typeof strTime === "undefined") {
        date = new Date();
    } else {
        date = new Date(strTime);
    }
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();
    if (Number(month) < 10) {
        month = "0" + month.toString()
    }
    if (Number(day) < 10) {
        day = "0" + day.toString()
    }
    if (Number(hour) < 10) {
        hour = "0" + hour.toString()
    }
    if (Number(minute) < 10) {
        minute = "0" + minute.toString()
    }
    if (Number(second) < 10) {
        second = "0" + second.toString()
    }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
};

/**
 * 时间格式化
 * hhmmss 或 hmmss >> hh:mm
 */
Tool.formatHourMinute = function (time) {
    time  = time +"";
    if (!isNaN(time) && time.length == 5 && time.indexOf(".") < 0) {
        time = "0" + time;
    } else if (!isNaN(time) && time.length == 4 && time.indexOf(".") < 0) {
        time = "00" + time;
    } else if (!isNaN(time) && time.length == 3 && time.indexOf(".") < 0) {
        time = "000" + time;
    } else if (!isNaN(time) && time.length == 2 && time.indexOf(".") < 0) {
        time = "0000" + time;
    } else if (!isNaN(time) && time.length == 1 && time.indexOf(".") < 0) {
        time = "00000" + time;
    }
    if (!isNaN(time) && time.length == 6 && time.indexOf(".") < 0) {
        time = time.replace(/^(\d{2})(\d{2})(\d{2})$/, "$1:$2");
    }

    return time;
};

/**
 * 日期格式化
 * yyyyMMdd >> yyyy-MM-dd
 */
Tool.formatDate = function (strDate) {
    strDate = strDate + "";
    if (!isNaN(strDate) && strDate.length == 8) {
        strDate = strDate.replace(/^(\d{4})(\d{2})(\d{2})$/, "$1-$2-$3");
    }
    return strDate;
};

/**
 * 时间格式化
 * hhmmss >> hh:mm:ss
 */
Tool.formatTime = function (time) {
    time  = time +"";
    if (!isNaN(time) && time.length == 5 && time.indexOf(".") < 0) {
        time = "0" + time;
    } else if (!isNaN(time) && time.length == 4 && time.indexOf(".") < 0) {
        time = "00" + time;
    } else if (!isNaN(time) && time.length == 3 && time.indexOf(".") < 0) {
        time = "000" + time;
    } else if (!isNaN(time) && time.length == 2 && time.indexOf(".") < 0) {
        time = "0000" + time;
    } else if (!isNaN(time) && time.length == 1 && time.indexOf(".") < 0) {
        time = "00000" + time;
    }
    if (!isNaN(time) && time.length == 6 && time.indexOf(".") < 0) {
        time = time.replace(/^(\d{2})(\d{2})(\d{2})$/, "$1:$2:$3");
    }
    return time;
};

/**
 * 日期格式化
 * yyyyMMdd >> yyyy.MM.dd
 */
Tool.formatDatePoint = function (strDate) {
    strDate = strDate + "";
    if (!isNaN(strDate) && strDate.length == 8) {
        strDate = strDate.replace(/^(\d{4})(\d{2})(\d{2})$/, "$1.$2.$3");
    }
    return strDate;
};

/**
 * 计算两个日期间隔天数
 * @param startTime 开始日期（毫秒）
 * @param endTime 结束日期（毫秒）
 * */
Tool.dateDiff = function (startTime, endTime) {
    let dates = (startTime - endTime) / (1000 * 60 * 60 * 24);
    return dates;
};

/**
 * 获取 endDate的前days天的日期
 * @param endDate   Date
 * @param days      int
 * @return {string}
 */
Tool.getStartDate = function (endDate, days) {
    let day = endDate.getDate();
    let tempDate, tempMouth, tempDay;
    if (day < days) {
        if (endDate.getMonth() - 1 < 0) {  //前一年
            tempDate = new Date(endDate.getFullYear() - 1, 11, 0);
            tempDay = tempDate.getDate() - days + day + 1;
            tempDay = tempDay < 10 ? '0' + tempDay : tempDay;
            return tempDate.getFullYear() + '' + 12 + tempDay;
        } else {  //当年
            tempDate = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
            tempDay = tempDate.getDate() - days + day + 1;
            tempDay = tempDay < 10 ? '0' + tempDay : tempDay;
            tempMouth = tempDate.getMonth() + 1;
            tempMouth = tempMouth < 10 ? '0' + tempMouth : tempMouth;
            return tempDate.getFullYear() + '' + tempMouth + tempDay;
        }
    } else if (day == days) {
        if (endDate.getMonth() - 1 < 0) {  //前一年
            tempDate = new Date(endDate.getFullYear() - 1, 11, 0);
            tempDay = tempDate.getDate() - days + day + 1;
            tempDay = tempDay < 10 ? '0' + tempDay : tempDay;
            return tempDate.getFullYear() + '' + 12 + tempDay;
        } else {  //当年
            tempDate = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
            tempDay = tempDate.getDate();
            tempDay = tempDay < 10 ? '0' + tempDay : tempDay;
            tempMouth = tempDate.getMonth() + 1;
            tempMouth = tempMouth < 10 ? '0' + tempMouth : tempMouth;
            return tempDate.getFullYear() + '' + tempMouth + tempDay;
        }
    } else if (day > days) {
        tempMouth = endDate.getMonth() + 1;
        tempMouth = tempMouth < 10 ? '0' + tempMouth : tempMouth;
        return endDate.getFullYear() + '' + tempMouth + (day - days < 10 ? '0' + (day - days) : day - days);
    }
};

/**
 * 格式化时间
 * 20000909 --> 2000-09-09
 */
Tool.dateFormat = function (date) {
    if (Tool.isEmpty(date)) {
        return "";
    }
    date = date + "";
    if (date.length != 8) {
        return date;
    }
    return date.substr(0, 4) + "-" + date.substr(4, 2) + "-" + date.substr(6, 2);
};

Tool.tapType = function () {
    let type = "click";
    if (Env.isAndroid() || Env.isIOS()) {
        type = "tap";
    }
    return type;
};

module.exports = Tool;