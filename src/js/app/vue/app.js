/**
 * Created by songshuohua on 2017/1/19.
 */
import {getNowTime} from '../../util/tool';
import HelloComp from './component/hello.vue';

/**
 * Vue.component 作用是将通过 Vue.extend 生成的扩展实例构造器注册（命名）为一个组件，在注册之后，便可以在父实例的模块中以自定义元素
 * <my-component></my-component> 的形式使用。可以简单理解为当在模板中遇到该定义元素时，会自动调用类似于 new myVue 这样的构造函数来生成组件实例，
 * 并挂载到自定义元素上，要确保在初始化根实例之前注册了组件。组件实例的作用域是孤立的，这意味着不能并且不应该在子组件的模板内直接引用父组件
 * （或 Vue 实例）的数据。可以使用字面量语法传递普通字符串，也可以在组件根元素通过 v-bind:*** 动态绑定 props 的值到父组件的数据中，
 * 并在组件模版中使用 props 把数据传给子组件。prop 是单向绑定的：当父组件的属性变化时，将传导给子组件，但是不会反过来。
 * 这是为了防止子组件无意修改了父组件的状态——这会让应用的数据流难以理解。HTML 特性不区分大小写，当使用非字符串模版时，prop的名字形式会从 camelCase
 * 转为 kebab-case（短横线隔开）。如果使用字符串模版，不用在意这些限。
 *
 * prop 是父组件用来传递数据的一个自定义属性，子组件需要显式地用 props 选项声明 “prop”。通常有两种改变 prop 的情况：
 * 1、prop 作为初始值传入，子组件之后只是将它的初始值作为本地数据的初始值使用；
 * 2、prop 作为需要被转变的原始值传入。
 * 注：在 JavaScript 中对象和数组是引用类型，指向同一个内存空间，如果 prop 是一个对象或数组，在子组件内部改变它会影响父组件的状态。
 *
 * 父组件是使用 props 传递数据给子组件，但如果子组件要把数据传递回去，应该怎样做？那就是自定义事件
 * 每个 Vue 实例都实现了事件接口(Events interface)，即：
 * 1、使用 $on(eventName) 监听事件
 * 2、使用 $emit(eventName) 触发事件
 * 注：Vue的事件系统分离自浏览器的 EventTarget API，尽管它们的运行类似，但是 $on 和 $emit 不是addEventListener 和 dispatchEvent 的别名。
 * 另外，父组件可以在使用子组件的地方（即所注册组件对应的元素）直接用 v-on 来监听子组件触发的事件。
 * 在本例中，子组件已经和它外部完全解耦了，它所做的只是触发一个父组件关心的内部事件。
 *
 * 父组件模板的内容在父组件作用域内编译；子组件模板的内容在子组件作用域内编译。一个常见错误是试图在父组件模板内将一个指令绑定到子组件的属性/方法：
 * <child-component v-show="someChildProperty">{{此处定义的为父组件的内容，非子组件}}</child-component>
 * 假定 someChildProperty 是子组件的属性，上例不会如预期那样工作。父组件模板不应该知道子组件的状态。
 * 如果要绑定子组件内的指令到改组件的根节点（即 HTML 中的 <todo-item></todo-item>），应当在它的模板（字符串模版或script元素中定义的模版）内这么做：
 * */
Vue.component('todo-item', {
    template: `
        <li v-bind:id="todo.id">
          {{index}} : {{todo.id}} : {{ todo.title }}
          <button v-on:click="remove">X</button>
          <input type="text" value="">
        </li>
    `,
    props: ['index', 'todo'],
    data: function () {// 定义一个局部 data 属性，并将 prop 的初始值作为局部数据的初始值
        return { counter: this.index };
    },
    computed: {// 定义一个 computed 属性，此属性从 prop 的值计算得出。
        normalizedTitle: function () {
            return this.title.trim().toLowerCase();
        }
    },
    methods: {
        remove: function(event) {
            this.$emit('remove');
        }
    }
});

/**
 * v-model 仅仅是一个语法糖，等价于：
 * <input v-bind:value="something" v-on:input="something = $event.target.value">
 * 所以在组件中使用时，它相当于下面的简写：
 * <custom-input v-bind:value="something" v-on:input="something = arguments[0]"></custom-input>
 * 所以要让组件的根元素（<custom-input>）上的 v-model 生效，模版必须：
 * 1、通过 props 接受一个 value 属性
 * 2、在有新的 value 时触发 input 事件
 *
 * 尽管有 props 和 events，但是有时仍然需要在 JavaScript 中直接访问子组件（等同子元素，可以是原生，也可以是自定义），
 * 为此可以使用 ref 为子组件指定一个索引 ID。当 ref 和 v-for 一起使用时，ref 是一个数组或对象，包含相应的子组件。
 * 注：$refs 只在组件渲染完成后才填充，并且它是非响应式的。它仅仅作为一个直接访问子组件的应急方案——应当避免在模版或计算属性中使用 $refs。
 * */
Vue.component('custom-input', {
    template: `
        <span>
            $
            <input
                ref="uniqueA"
                v-bind:value="value"
                v-on:input="updateValue($event.target.value)" />
        </span>
    `,
    props: ['value'],
    methods: {
        updateValue: function(value) {
            let formattedValue = value.trim().slice(0, value.indexOf('.') + 3); // 删除两侧的空格符并保留 2 小数位
            if (formattedValue !== value) {// 如果值不统一，手动覆盖以保持一致
                this.$refs.uniqueA.value = formattedValue
            }
            /*
            * 通过 input 事件发出数值，此时触发的是 <custom-input> 元素中绑定的 input 事件（因未加 .native 后缀，故作为自定义事件处理），
            * 非模板中，<input> 元素绑定的 input 事件（原生事件）。此处的 this 指向内存中的组件实例（custom-input）
            * this.$emit 提交的是当前 Vue 实例（或组件）上绑定的自定义事件，非原生事件，不会触发上层组件的事件
            * */
            this.$emit('input', Number(formattedValue));
        }
    }
});

/**
 * 在大型应用中，我们可能需要将应用拆分为多个小模块，按需从服务器下载。为了让事情更简单，Vue.js 允许将组件定义为一个工厂函数，
 * 动态地解析组件的定义。Vue.js 只在组件需要渲染时触发工厂函数，并且把结果缓存起来，用于后面的再次渲染。
 * 工厂函数接收一个 resolve 回调，在收到从服务器下载的组件定义时调用。也可以调用 reject(reason) 指示加载失败。
 * 这里 setTimeout 只是为了演示。怎么获取组件完全由你决定。推荐配合使用 ：Webpack 的代码分割功能：
 * */
Vue.component("async-example", function(resolve, reject) {
    setTimeout(function () {
        // Pass the component definition to the resolve callback
        resolve({
            template: '<div>I am async!</div>'
        })
    }, 3000)
});

/**
 * 也可以使用 Webpack 2 + ES2015 的语法返回一个 Promise resolve 函数。
 * 注：如果你是 Browserify 用户,可能就无法使用异步组件了,它的作者已经表明 Browserify 是不支持异步加载的。
 * Browserify 社区发现一些解决方法（https://github.com/vuejs/vuejs.org/issues/620），可能有助于已存在的复杂应用。
 * 对于其他场景，我们推荐简单实用 Webpack 构建，一流的异步支持
 * */
Vue.component(
    'async-webpack-example',
    () => System.import('./my-async-component')
);

/**
 * 为了让组件可以组合，我们需要一种方式来混合父组件的内容（即 HTML 页面中，在子组件元素中定义的内容，
 * 如<app-layout>此处为父组件的内容，并在父组件作用域编译</app-layout>）与子组件自己的模板。这个过程被称为内容分发
 * (或 “transclusion” 如果你熟悉 Angular)。Vue.js 使用特殊的 <slot> 元素作为原始内容的插槽，参照了当前 Web 组件规范草案。
 * <slot> 元素可以用一个特殊的属性 name 来配置如何分发内容。多个 slot 可以有不同的名字。具名 slot 将匹配父组件内容片段中有对应 slot 特性的元素。
 * 仍然可以有一个匿名 slot，它是默认 slot，作为找不到匹配的内容片段的备用插槽。如果没有默认的 slot，这些找不到匹配的内容片段将被抛弃。
 * */
Vue.component("app-layout", {
    template: `
        <div class="container">
          <header>
            <slot name="header"></slot>
          </header>
          <main>
            <slot text="我是子组件模版中，通过 slot 绑定的属性"></slot>
          </main>
          <footer>
            <slot name="footer"></slot>
          </footer>
        </div>
    `
});

/**
 * 作用域插槽是一种特殊类型的插槽，用作使用一个（能够传递数据到）可重用模板替换已渲染元素。在子组件中，
 * 只需将数据传递到插槽（可以是静态字符串，或通过v-bind 传递动态属性），就像你将 prop 传递给组件一样：
 * */
Vue.component("my-awesome-list", {
    template: `
        <ul>
            <slot name="item"
                v-for="item in items"
                :message="item.message">
                <!-- fallback content here -->
          </slot>
        </ul>
    `,
    props:['items']
});

/**
 * 组件可以嵌套调用
 * */
Vue.component("recursion-component", {
    template: `
        <div>
            <slot>
                <div>默认插槽</div>
            </slot>
            <slot name="foo">
                <div>具名插槽</div>
            </slot>
        </div>
    `
});

/**
 *
 * */
Vue.component("hello-world", {
    template: '#hello-world-template'
});

/**
 * 尽管在 Vue 中渲染 HTML 很快，不过当组件中包含大量静态内容时，可以考虑使用 v-once 将渲染结果缓存起来
 * */
Vue.component('terms-of-service', {
    template: `
        <div v-once>
            <h6>Terms of Service</h6>
            ... a lot of static content ...
        </div>
    `
});

/**
 * 过渡可以通过 Vue 的组件系统实现复用。要创建一个可复用过渡组件，你需要做的就是将
 * <transition> 或者 <transition-group> 作为根组件，然后将任何子组件放置在其中就可以了
 * */
Vue.component('my-special-transition', {
    functional: true,
    render: function (createElement, context) {
        var data = {
            props: {
                name: 'very-special-transition',
                mode: 'out-in'
            },
            on: {
                beforeEnter: function (el) {
                    // ...
                },
                afterEnter: function (el) {
                    // ...
                }
            }
        };
        return createElement('transition', data, context.children)
    }
});

/**
 * 这种复杂的补间动画逻辑可以被复用,任何整数都可以执行动画，组件化使我们的界面十分清晰，可以支持更多更复杂的动态过渡策略
 * */
Vue.component('animated-integer', {
    // 为了制造补间动画效果，此处不直接绑定 value 属性，在动画执行完毕，再将 value 的值赋给 tweeningValue
    template: '<span>{{ tweeningValue }}</span>',
    props: {
        value: {
            type: Number,
            required: true
        }
    },
    data: function () {
        return {
            tweeningValue: 0
        }
    },
    watch: {
        value: function (newValue, oldValue) {
            this.tween(oldValue, newValue);
        }
    },
    mounted: function () {
        this.tween(0, this.value)
    },
    methods: {
        tween: function (startValue, endValue) {
            var vm = this;
            function animate (time) {
                requestAnimationFrame(animate)
                TWEEN.update(time)
            }
            new TWEEN.Tween({ tweeningValue: startValue })
                .to({ tweeningValue: endValue }, 500)
                .onUpdate(function () {
                    vm.tweeningValue = this.tweeningValue.toFixed(0);
                })
                .start();
            animate();
        }
    }
})

/**
 * 使用组件时，大多数可以传入到 Vue 构造器中的选项可以在注册组件时使用，有一个例外：data 必须是函数。
 * 因为组件可以被复用，如果传的是对象，多次使用时会互相影响
 * */
let Child = {
    template: '<button v-on:click="counter++">自定义计数器：{{counter}}</button>',
    data: function () {
        /*
         * 如果此处返回的是在外部定义的对象，Vue 不会警告，但是我们为每一个组件返回了同一个对象引用，依然会出问题
         * 我们可以通过为每个组件返回新的 data 对象来解决这个问题，现在每个 counter 都有它自己内部的状态了：
         * */
        return {
            counter: 0
        }
    }
};

/**
 * 有时候非父子关系的组件也需要通信。在简单的场景下，使用一个空的 Vue 实例作为中央事件总线：
 * 在更多复杂的情况下，你应该考虑使用专门的 状态管理模式.
 * */
var bus = new Vue();

// 触发组件 A 中的事件
bus.$emit('id-selected', 1);

// 在组件 B 创建的钩子中监听事件
bus.$on('id-selected', function (id) {
    // ...
});

/**
 * 在 Vue2.0 里面，代码复用的主要形式和抽象是组件——然而，有的情况下,你仍然需要对纯 DOM 元素进行底层操作,这时候就会用到自定义指令
 *
 * 指令定义函数提供了几个钩子函数（可选）：
 * bind: 只调用一次，指令第一次绑定到元素时调用，用这个钩子函数可以定义一个在绑定时执行一次的初始化动作。
 * inserted: 被绑定元素插入父节点时调用（父节点存在即可调用，不必存在于 document 中）。
 * update: 被绑定元素所在的模板更新时调用，而不论绑定值是否变化。通过比较更新前后的绑定值，可以忽略不必要的模板更新（详细的钩子函数参数见下）。
 * componentUpdated: 被绑定元素所在模板完成一次更新周期时调用。
 * unbind: 只调用一次， 指令与元素解绑时调用。
 *
 * 钩子函数被赋予了以下参数：
 * el: 指令所绑定的元素，可以用来直接操作 DOM 。
 * binding: 一个对象，包含以下属性：
 *     name: 指令名，不包括 v- 前缀。
 *     value: 指令的绑定值， 例如： v-my-directive="1 + 1", value 的值是 2。
 *     oldValue: 指令绑定的前一个值，仅在 update 和 componentUpdated 钩子中可用。无论值是否改变都可用。
 *     expression: 绑定值的字符串形式。 例如 v-my-directive="1 + 1" ， expression 的值是 "1 + 1"。
 *     arg: 传给指令的参数。例如 v-my-directive:foo， arg 的值是 "foo"。
 *     modifiers: 一个包含修饰符的对象。 例如： v-my-directive.foo.bar, 修饰符对象 modifiers 的值是 { foo: true, bar: true }。
 * vnode: Vue 编译生成的虚拟节点，查阅 VNode API 了解更多详情。
 * oldVnode: 上一个虚拟节点，仅在 update 和 componentUpdated 钩子中可用。
 *
 * 除了 el 之外，其它参数都应该是只读的，尽量不要修改他们。如果需要在钩子之间共享数据，建议通过元素的 dataset 来进行。
 * */
Vue.directive('demo', {
    bind: function (el, binding, vnode, oldVnode) {
        var s = JSON.stringify;
        el.innerHTML =
            'name: '       + s(binding.name) + '<br>' +
            'value: '      + s(binding.value) + '<br>' +
            'expression: ' + s(binding.expression) + '<br>' +
            'argument: '   + s(binding.arg) + '<br>' +
            'modifiers: '  + s(binding.modifiers) + '<br>' +
            'vnode keys: ' + Object.keys(vnode).join(', ')
    },
    inserted: function(el, binding, vnode, oldVnode) {
        console.log("自定义指令：inserted");
    },
    update: function(el, binding, vnode, oldVnode) {// 为何页面加载后，update 及 componentUpdated 一直在执行？
        // console.log("自定义指令：update");
    },
    componentUpdated: function(el, binding, vnode, oldVnode) {
        // console.log("自定义指令：componentUpdated");
    },
    unbind: function(el, binding, vnode, oldVnode) {
        console.log("自定义指令：unbind");
    }
});

/**
 * 定义一个混合对象
 * 当组件和混合对象含有同名选项时，这些选项将以恰当的方式混合：
 * 1、同名钩子函数将混合为一个数组，因此都将被调用。另外，混合对象的钩子将在组件自身钩子之前调用
 * 2、值为对象的选项，例如 methods, components 和 directives，将被混合为同一个对象。两个对象键名冲突时，取组件对象的键值对。
 *
 * 也可以全局注册混合对象。 注意使用！ 一旦使用全局混合对象，将会影响到 所有 之后创建的 Vue 实例。使用恰当时，可以为自定义对象注入处理逻辑。
 * */
let myMixin = {
    created: function () {
        this.hello()
    },
    methods: {
        hello: function () {
            console.log('hello from mixin!')
        }
    }
};

// 组件必须定义在混合对象的后面
Vue.component("my-mixin", {
    template: '<div>必须有 template 属性，才有挂载点</div>',
    mixins: [myMixin]
});

/**
 * 初始化根实例
 * */
let vm = new Vue({
    el: '#example',
    components: {
        // <my-component> 将只在父模板可用
        'child': Child,
        home: {
            template: '<div class="a">this is Home component</div>' // 模板字符串中，必须有根元素，并且不能为保留元素（<slot> 等）否则不会渲染
        },
        archive: {
            template: '<div>this is archive component</div>'
        },
        helloComp: {
            render: h => h(HelloComp)
        }
        // helloComp: HelloComp
    },
    data: {
        message: 'hello, world ',
        rawHtml: '<h5>我是 HTML H5 标记</h5>',
        rawId: '20170130',
        number: 12,
        disabledButton: false,
        isVisible: true,
        url: 'https://www.baidu.com',
        question: '',
        answer: 'I cannot give you an answer until you ask a question!',
        firstName: 'Foo',
        lastName: 'Bar',
        loginType: 'username',
        parentMessage: 'Parent',
        items: [
            {message: 'Foo' },
            {message: 'Bar' }
        ],
        person: {
            FirstName: 'John',
            LastName: 'Doe',
            Age: 30
        },
        newTodoText: '',
        todos: [
            {id: getNowTime(), title: 'Do the dishes'},
        ],
        numbers: [ 1, 2, 3, 4, 5 ],
        checkedNames: [], // 如果是单个复选框，此处应为字符串
        picked: 'Two',
        selected: [{ text: 'Two', value: 'B' }, { text: 'Three', value: 'C' }], // 如果是单选下拉菜单，此处应为字符串
        options: [
            { text: 'One', value: 'A' },
            { text: 'Two', value: 'B' },
            { text: 'Three', value: 'C' }
        ],
        price: 22.233,
        currentView: 'home',
        nextNum: 10,
        cells: Array.apply(null, { length: 81 })
            .map(function (_, index) {
                return {
                    id: index, number: index % 9 + 1
                }
            }),
        query: '',
        list: [
            { msg: 'Bruce Lee' },
            { msg: 'Jackie Chan' },
            { msg: 'Chuck Norris' },
            { msg: 'Jet Li' },
            { msg: 'Kung Fury' }
        ],
        show: true,
        fadeInDuration: 1000,
        fadeOutDuration: 1000,
        maxFadeDuration: 1500,
        stop: false,
        animatedNumber: 0,
        colorQuery: '',
        color: {
            red: 0,
            green: 0,
            blue: 0,
            alpha: 1
        },
        tweenedColor: {},
        firstNumber: 20,
        secondNumber: 40
    },
    /**
     * methods 中的函数，必须要有一定的触发条件。一般作为页面事件的回调，或者在 watch 属性中调用。
     * 如果在页面中通过 mustache 调用，必须加上 "()"，否则返回定义的函数
     * */
    methods: {
        greet: function(event) {
            alert(event.target.innerText);
        },
        onSubmit: function() {
            console.log("提交表单的回调函数");
        },
        getAnswer: _.debounce(
            function () {
                let vm = this;
                if (this.question.indexOf('?') === -1) {
                    vm.answer = 'Questions usually contain a question mark. ;-)'
                    return
                }
                vm.answer = 'Thinking...'
                axios.get('https://yesno.wtf/api')
                    .then(function (response) {
                        vm.answer = _.capitalize(response.data.answer)
                    })
                    .catch(function (error) {
                        vm.answer = 'Error! Could not reach the API. ' + error
                    })
            },
            // 这是我们为用户停止输入等待的毫秒数
            500
        ),
        addNewTodo: function () {
            let todo = {};
            todo.id = getNowTime();
            todo.title = this.newTodoText;

            this.todos.push(todo);
            this.newTodoText = '';

            // 在组件内使用 vm.$nextTick() 实例方法特别方便，因为它不需要全局 Vue ，并且回调函数中的 this 将自动绑定到当前的 Vue 实例上：
            console.log("dom未更新，子元素还是原来的数量", this.$el.querySelector('.todoItems').children.length);
            this.$nextTick(function () {
                console.log("dom已更新", this.$el.querySelector('.todoItems').children.length);
            })
        },
        /**
         * Vue 包含一组观察数组的变异方法，会改变被这些方法调用的原始数组，所以它们也将会触发视图更新。这些方法如下：
         * push()
         * pop()
         * shift()
         * unshift()
         * splice()
         * sort()
         * reverse()
         * 也有非变异(non-mutating method)方法，例如： filter(), concat(), slice() 。
         * 这些不会改变原始数组，但总是返回一个新数组。当使用非变异方法时，可以用新数组替换旧数组。
         * 由于 JavaScript 的限制，Vue 不能检测以下变动的数组：
         * 1、当你利用索引直接设置一个项时，例如： vm.items[indexOfItem] = newValue
         * 2、当你修改数组的长度时，例如： vm.items.length = newLength
         * 为了避免第一种情况，以下两种方式将达到像 vm.items[indexOfItem] = newValue 的效果， 同时也将触发状态更新：
         * 1、Vue.set(example1.items, indexOfItem, newValue)
         * 2、example1.items.splice(indexOfItem, 1, newValue)
         * 避免第二种情况，使用 splice：
         * example1.items.splice(newLength)
         *
         * 每个组件实例都有相应的 watcher 实例对象，它会在组件渲染的过程中把属性记录为依赖，
         * 之后当依赖项的 setter 被调用时，会通知 watcher 重新计算，从而致使它关联的组件得以更新。
         *
         * Vue 不允许在已经创建的实例上动态添加新的根级响应式属性(root-level reactive property)。
         * 然而它可以使用 Vue.set(object, key, value) 方法将响应属性添加到嵌套的对象上：
         * 1、Vue.set(vm.someObject, 'b', 2)
         * 您还可以使用 vm.$set 实例方法，这也是全局 Vue.set 方法的别名：
         * 1、this.$set(this.someObject,'b',2)
         * 有时你想向已有对象上添加一些属性，例如使用 Object.assign() 或 _.extend() 方法来添加属性。
         * 但是，添加到对象上的新属性不会触发更新，如：
         * 1、`Object.assign(this.someObject, { a: 1, b: 2 })`
         * 在这种情况下可以创建一个新的对象，让它包含原对象的属性和新的属性：
         * 1、this.someObject = Object.assign({}, this.someObject, { a: 1, b: 2 })
         *
         * 如果你在模版中访问了 data 中未声明的属性，Vue 将警告你渲染函数在试图访问的属性不存在。
         * 这样的限制在背后是有其技术原因的，它消除了在依赖项跟踪系统中的一类边界情况，也使 Vue 实例在类型检查系统的帮助下运行的更高效。
         * 而且在代码可维护性方面也有一点重要的考虑：data 对象就像组件状态的概要，提前声明所有的响应式属性，
         * 可以让组件代码在以后重新阅读或其他开发人员阅读时更易于被理解。
         *
         * */
        shuffle: function () {
            this.todos = _.shuffle(this.todos);
        },
        even: function (numbers) {
            return numbers.filter(function (number) {
                return number % 2 === 0
            })
        },
        spliceTodos: function(index) {
            this.todos.splice(index, 1)
        },
        randomIndex: function () {
            return Math.floor(Math.random() * this.numbers.length)
        },
        add: function () {
            this.numbers.splice(this.randomIndex(), 0, this.nextNum++)
        },
        remove: function () {
            this.numbers.splice(this.randomIndex(), 1)
        },
        shuffleNumbers: function () {
            this.numbers = _.shuffle(this.numbers);
        },
        shuffleCells: function () {
            this.cells = _.shuffle(this.cells)
        },
        /*
        * 这些钩子函数可以结合 CSS transitions/animations 使用，也可以单独使用。
        * 当只用 JavaScript 过渡的时候，在 enter 和 leave 中，回调函数 done 是必须的。否则，它们会被同步调用，过渡会立即完成。
        * 推荐对于仅使用 JavaScript 过渡的元素添加 v-bind:css="false"，Vue 会跳过 CSS 的检测。这也可以避免过渡过程中 CSS 的影响。
        * */
        beforeAppear: function (el) {
            console.log("beforeEnter", el);
        },
        appear: function (el, done) {
            console.log("appear", el);
            done();
        },
        afterAppear: function (el) {
            console.log("afterAppear", el);
        },
        beforeEnter: function (el) {
            el.style.opacity = 0;
            el.style.transformOrigin = 'left';

            console.log("beforeEnter", el);
        },
        // 此回调函数是可选项的设置，与 CSS 结合时使用
        enter: function (el, done) {
            // 如果没有引入 jQuery，Velocity.js 会增加 Velocity 作为全局变量。繁反之，必须通过 jQuery 选择器使用 Velocity
            // Velocity(el, { opacity: 1, fontSize: '1.4em' }, { duration: 300 });
            // Velocity(el, { fontSize: '1em' }, { complete: done });
            $(el).velocity({ opacity: 1, fontSize: '1.4em' }, { duration: 300 });
            $(el).velocity({ fontSize: '1em' }, { complete: done });

            console.log("enter", el);
            // done(); // Velocity 中已经指定 { complete: done }，此处重复调用会导致过渡立即完成
        },
        afterEnter: function (el) {
            console.log("afterEnter", el);
        },
        enterCancelled: function (el) {
            console.log("enterCancelled", el);
        },
        beforeLeave: function (el) {
            console.log("beforeLeave", el);
        },
        // 此回调函数是可选项的设置，与 CSS 结合时使用
        leave: function (el, done) {
            // Velocity(el, { translateX: '15px', rotateZ: '50deg' }, { duration: 600 });
            // Velocity(el, { rotateZ: '100deg' }, { loop: 2 });
            // Velocity(el, {
            //     rotateZ: '45deg',
            //     translateY: '30px',
            //     translateX: '30px',
            //     opacity: 0
            // }, { complete: done });

            $(el).velocity({ translateX: '15px', rotateZ: '50deg' }, { duration: 600 });
            $(el).velocity({ rotateZ: '100deg' }, { loop: 2 });
            $(el).velocity(
                {
                    rotateZ: '45deg',
                    translateY: '30px',
                    translateX: '30px',
                    opacity: 0
                },
                {
                    complete: done
                }
            );

            console.log("leave", el);
            // done();
        },
        afterLeave: function (el) {
            console.log("afterLeave", el);
        },
        // leaveCancelled 只用于 v-show 中
        leaveCancelled: function (el) {
            console.log("leaveCancelled", el);
        },
        customBeforeEnter: function (el) {
            el.style.opacity = 0;
            el.style.height = 0;
        },
        customEnter: function (el, done) {
            var delay = el.dataset.index * 150;
            setTimeout(function () {
                $(el).velocity(
                    { opacity: 1, height: '1.6em' },
                    { complete: done }
                )
            }, delay)
        },
        customLeave: function (el, done) {
            var delay = el.dataset.index * 150;
            setTimeout(function () {
                $(el).velocity(
                    { opacity: 0, height: 0 },
                    { complete: done }
                )
            }, delay)
        },
        dynamicBeforeEnter: function (el) {
            el.style.opacity = 0
        },
        dynamicEnter: function (el, done) {
            var vm = this;
            $(el).velocity(
                { opacity: 1 },
                {
                    duration: this.fadeInDuration,
                    complete: function () {
                        done();
                        if (!vm.stop) vm.show = false
                    }
                }
            )
        },
        dynamicLeave: function (el, done) {
            var vm = this;
            $(el).velocity(
                { opacity: 0 },
                {
                    duration: this.fadeOutDuration,
                    complete: function () {
                        done();
                        vm.show = true
                    }
                }
            )
        },
        updateColor: function () {
            this.color = new net.brehaut.Color(this.colorQuery).toRGB();
            this.colorQuery = ''
        }
    },
    filters: {
        capitalize: function(value) {
            if (!value) {
                return '';
            }
            value = value.toString();
            return value.charAt(0).toUpperCase() + value.slice(1);

        },
        addSuffix: function(value) {
            if (!value) {
                return '';
            }
            let arg1 = !arguments[1] == true ? '': arguments[1];
            let arg2 = !arguments[2] == true ? '': arguments[2];
            value = value.toString() + "-----" + arg1 + arg2;
            return value;
        }
    },
    /**
     * 计算属性是在 dom 加载后马上执行的。所提供的函数不支持异步操作
     * */
    computed: {
        /**
         * 我们提供的函数将用作属性 vm.reversedMessage 的 getter。Vue 知道 vm.reversedMessage 依赖于 vm.message，
         * 因此当 vm.message 发生改变时，依赖于 vm.reversedMessage 的绑定也会更新。
         * 而且最妙的是我们是声明式地创建这种依赖关系：计算属性的 getter 是干净无副作用的，因此也是易于测试和理解的。
         * */
        reversedMessage: function() {
            return this.message.split('').reverse().join('');
        },
        /**
         * 不经过计算属性，我们可以在 Vue 的 methods 属性中定义一个相同的函数来替代它(reversedMessage)。对于最终的结果，两种方式确实是相同的。
         * 然而，不同的是计算属性是基于它的依赖缓存。计算属性只有在它的相关依赖发生改变时才会重新取值。这也同样意味着如下计算属性将不会更新，
         * 因为 Date.now() 不是响应式依赖。相比而言，每当重新渲染的时候，method 调用总会执行函数。我们为什么需要缓存？假设我们有一个重要的计算属性 A，
         * 这个计算属性需要一个巨大的数组遍历和做大量的计算，然后我们可能有其他的计算属性依赖于 A。如果没有缓存，我们将不可避免的多次执行 A 的 getter ！
         * 如果你不希望有缓存，请用 method 替代。
         * */
        now: function() {
            return Date.now();
        },
        /**
         * 计算属性默认只有 getter，不过在需要时你也可以提供一个 setter。现在在运行 vm.fullName = 'John Doe' 时，
         * setter 会被调用，vm.firstName 和 vm.lastName 也会被对应更新。
         * */
        fullName: {
            // getter
            get: function () {
                return this.firstName + ' ' + this.lastName
            },
            // setter
            set: function (newValue) {
                let names = newValue.split(' ');
                this.firstName = names[0];
                this.lastName = names[names.length - 1];
            }
        },
        evenNumbers: function () {
            return this.numbers.filter(function (number) {
                return number % 2 === 0
            })
        },
        computedList: function () {
            var vm = this;
            return this.list.filter(function (item) {
                return item.msg.toLowerCase().indexOf(vm.query.toLowerCase()) !== -1
            })
        },
        tweenedCSSColor: function () {
            return new net.brehaut.Color({
                red: this.tweenedColor.red,
                green: this.tweenedColor.green,
                blue: this.tweenedColor.blue,
                alpha: this.tweenedColor.alpha
            }).toCSS();
        },
        result: function () {
            return this.firstNumber + this.secondNumber
        }
    },
    /**
     * Vue.js 提供了一个方法 $watch ，它用于观察 Vue 实例上的数据变动。当一些数据需要根据其它数据变化时，
     * $watch 很诱人 —— 特别是如果你来自 AngularJS 。不过，通常更好的办法是使用计算属性而不是一个命令式的 $watch 回调
     * 虽然计算属性在大多数情况下更合适，但有时也需要一个自定义的 watcher 。这是为什么 Vue 提供一个更通用的方法通过 watch 选项，
     * 来响应数据的变化。当你想要在数据变化响应时，执行异步操作或开销较大的操作，这是很有用的
     * */
    watch: {
        // 如果 question 发生改变，这个函数就会运行
        question: function (newQuestion) {
            this.answer = 'Waiting for you to stop typing...'
            this.getAnswer();
        },
        number: function(newValue, oldValue) {
            var vm = this;
            function animate (time) {
                requestAnimationFrame(animate);
                TWEEN.update(time);
            }
            new TWEEN.Tween({ tweeningNumber: oldValue })
                .easing(TWEEN.Easing.Quadratic.Out)
                .to({ tweeningNumber: newValue }, 500)
                .onUpdate(function () {
                    vm.animatedNumber = this.tweeningNumber.toFixed(0); // 此处为异步回调，故 this 非指向当前 Vue 实例
                })
                .start();
            animate();
        },
        color: function () {
            function animate (time) {
                requestAnimationFrame(animate);
                TWEEN.update(time)
            }
            new TWEEN.Tween(this.tweenedColor) // 此处直接使用 this.tweenedColor 获取 vm.$data 中的属性，动画执行过程中也会直接修改该属性值
                .to(this.color, 750)
                .start();
            animate();
        }
    },
    created: function() {
        this.tweenedColor = Object.assign({}, this.color)
        // console.log("created：", this.message);
        console.log("-------------");
    },
    mounted: function() {
        this.show = false; // 挂在完成后，data.show 重新赋值
        console.log("mounted：", this.message);
        console.log("++++++");
    },
    updated: function() {
        // console.log("updated：", this.message);
    },
    destroyed: function() {
        console.log("destroyed：", this.message);
    }
});

/**
 * Vue 异步执行 DOM 更新。只要观察到数据变化，Vue 将开启一个队列，并缓冲在同一事件循环中发生的所有数据改变。如果同一个 watcher 被多次触发，
 * 只会一次推入到队列中。这种在缓冲时去除重复数据对于避免不必要的计算和 DOM 操作上非常重要。然后，在下一个的事件循环“tick”中，
 * Vue 刷新队列并执行实际（已去重的）工作。Vue 在内部尝试对异步队列使用原生的 Promise.then 和 MutationObserver，如果执行环境不支持，
 * 会采用 setTimeout(fn, 0) 代替。例如，当你设置 vm.someData = 'new value' ，该组件不会立即重新渲染。当刷新队列时，
 * 组件会在事件循环队列清空时的下一个“tick”更新。虽然 Vue.js 通常鼓励开发人员沿着“数据驱动”的方式思考，避免直接接触 DOM，
 * 但是有时我们确实需要在 DOM 状态更新后做点什么。为了在数据变化之后等待 Vue 完成更新 DOM ，可以在数据变化之后立即使用 Vue.nextTick(callback)。
 * 这样回调函数在 DOM 更新完成后就会调用。
 * */
vm.message = 'new message'; // 更改数据
console.log("after update data：", vm.$el.children[0].textContent === 'new message'); // false
Vue.nextTick(function () {
    console.log("after nextTick：", vm.$el.children[0].textContent === 'new message'); // true
});

vm.$watch('message', function(newVal, oldVal) {
    console.log("newVal", newVal);
    console.log("oldVal", oldVal);
});

// 压缩后 vm 变为局部属性，此处暴露为全局属性
window.vm = vm;