/**
 * Created by songshuohua on 2017/1/21.
 */
import Comp1 from './component/comp1';
import Comp2 from './component/comp2';

let App = (function () {
    /**
     * @class App
     * @constructor
     */
    function App() {
    }

    /**
     * App 实例
     * @property _instance
     * @type {App}
     * @default null
     * @private
     * @static
     */
    App._instance = null;

    /**
     * @static
     * @method getInstance
     * @public
     */
    App.getInstance = function () {
        if (App._instance === null) {
            App._instance = new App();
        }
        return App._instance;
    };

    /**
     * @method start
     * @public
     */
    App.prototype.start = function(){
        let self = this;
        self._init();
    };

    /**
     * @method start
     * @public
     */
    App.prototype._init = function(){
        new Comp1().render({}, "#comp1");
        new Comp2().render({}, "#comp2");
    };

    return App;
})();

let app = App.getInstance();
app.start();
