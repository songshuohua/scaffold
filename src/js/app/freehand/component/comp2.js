import Component from "../../../util/Component";
import template from "./comp2.template";

/**
 * @class src.js.app.freehand.component.comp2
 */
var Comp = Component.extend({

    /**
     * 延迟渲染
     * @property deferRender
     * @type {Boolean}
     */
    deferRender: false,

    /**
     * @class src.js.app.freehand.component.comp2
     * @constructor
     */
    initialize: function(option){
        Component.prototype.initialize.call(this, option)
    }
});


/**
 * 预编译后的模板渲染成html
 * @method template
 * @param  {any} data
 * @return {string} html
 * */
Comp.prototype.template= function(data){
    return template(data);
};

/**
 * 界面渲染前执行方法，可重写。
 * @method beforeRender
 * @public
 */
Comp.prototype.beforeRender = function () {
};


/**
 * 界面渲染后执行方法，渲染。
 * @method render
 * @public
 */
Comp.prototype.render = function (data, to) {
    var self = this;
    self.data = data;

    Component.prototype.render.call(this, data, to);
    return this;
};

/**
 * 界面渲染后执行方法，可重写。
 * @method afterRender
 * @public
 */
Comp.prototype.afterRender = function () {
};

module.exports = Comp;