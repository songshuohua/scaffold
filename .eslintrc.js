module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "node": true,
        "es6": true
    },
    // "extends": "eslint:recommended", // all of the rules marked “Tick” on the rules page will be turned on.
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
    },
    "plugins": [
        "react"
    ],
    "rules": {
        // Strict Mode
        "strict": "warn", // 要求或禁止使用严格模式指令
        // Possible Errors
        "no-cond-assign": "warn",//禁止条件表达式中出现赋值操作符
        "no-console": "warn",//禁用 console
        "no-constant-condition": "warn",//禁止在条件中使用常量表达式
        "no-control-regex": "warn",//禁止在正则表达式中使用控制字符
        "no-debugger": "warn",//禁用 debugger
        "no-dupe-args": "warn",//禁止 function 定义中出现重名参数
        "no-dupe-keys": "warn",//禁止对象字面量中出现重复的 key
        "no-duplicate-case": "warn",//禁止出现重复的 case 标签
        "no-empty-character-class": "warn",//禁止在正则表达式中使用空字符集
        "no-empty": "warn",//禁止出现空语句块
        "no-ex-assign": "warn",//禁止对 catch 子句的参数重新赋值
        "no-extra-boolean-cast": "warn",//禁止不必要的布尔转换
        "no-extra-parens": "warn",//禁止不必要的括号
        "no-extra-semi": "warn",//禁止不必要的分号
        "no-func-assign": "warn",//禁止对 function 声明重新赋值
        "no-inner-declarations": "warn",//禁止在嵌套的块中出现变量声明或 function 声明
        "no-invalid-regexp": "warn",//禁止 RegExp 构造函数中存在无效的正则表达式字符串
        "no-irregular-whitespace": "warn",//禁止在字符串和注释之外不规则的空白
        "no-obj-calls": "warn",//禁止把全局对象作为函数调用
        "no-prototype-builtins": "warn",//禁止直接调用 Object.prototypes 的内置属性
        "no-regex-spaces": "warn",//禁止正则表达式字面量中出现多个空格
        "no-sparse-arrays": "warn",//禁用稀疏数组
        "no-template-curly-in-string": "warn", //disallow template literal placeholder syntax in regular strings
        "no-unexpected-multiline": "warn",//禁止出现令人困惑的多行表达式
        "no-unreachable": "warn", //禁止在return、throw、continue 和 break 语句之后出现不可达代码
        "no-unsafe-finally": "warn",//禁止在 finally 语句块中出现控制流语句
        "no-unsafe-negation": "warn",//disallow negating the left operand of relational operators
        "use-isnan": "warn", //要求使用 isNaN() 检查 NaN
        "valid-jsdoc": "off", //强制使用有效的 JSDoc 注释
        "valid-typeof" :"warn", //强制 typeof 表达式与有效的字符串进行比较
        // Best Practices
        "no-case-declarations": "warn",// 不允许在 case 子句中使用词法声明
        "no-empty-pattern": "warn", // 禁止使用空解构模式
        "no-fallthrough": "warn", // 禁止 case 语句落空
        "no-octal": "warn", // 禁用八进制字面量
        "no-redeclare": "warn", // 禁止多次声明同一变量
        "no-self-assign": "warn", // 禁止自我赋值
        "no-unused-labels": "warn", // 禁用出现未使用过的标
        // Variables
        "no-delete-var": "warn", //
        "no-undef": "warn", //
        "no-unused-vars": "warn", //
    },
    "globals": {
        "$": true,
        "_": true
    }
};