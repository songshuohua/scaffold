var gulp = require('gulp');

/*
* NodeJS 子进程提供了与系统交互的重要接口，其主要API有： 标准输入、标准输出及标准错误输出的接口。
* child_process即子进程可以创建一个系统子进程并执行shell命令，在与系统层面的交互上挺有用处
* */
gulp.task('ipconfig', function(cb) {
    var exec = require('child_process').exec;
    var result = exec('ipconfig', function(err) {
        if (err) {
            console.log('回调执行出错');
            return cb(err); // 返回 error
        } else {
            console.log('回调执行结束');
            cb(); // 完成 task
        }
    });

    result.stdout.on('data', function (data) {
        console.log('标准输出：' + data);
    });

    result.on('exit', function (code) {
        console.log('子进程已关闭，代码：' + code);
    });
});

// 引入组件
var path = require('path'); // node自带组件
var fse = require('fs-extra'); // 通过npm下载

// 获取当前文件路径
var PWD = process.env.PWD || process.cwd(); // 兼容windows

gulp.task('init', function() {
    var dirs = ['dist','dist/html','dist/css','dist/img','dist/js','src','src/sass','src/js','src/img','src/pic','src/sprite','psd','.babelrc'];

    dirs.forEach(function (item,index) {
        // 使用mkdirSync方法新建文件夹
        fse.mkdirSync(path.join(PWD + '/'+ item));
    });

    // 往index里写入的基本内容
    var template = '<!DOCTYPE html><html lang="zh-CN"><head><meta charset="utf-8"/><meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"/><title></title><meta name="apple-touch-fullscreen" content="yes" /><meta name="format-detection" content="telephone=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /></head><body></body></html>';

    fse.writeFileSync(path.join(PWD + '/dist/html/index.html'), template);
    fse.writeFileSync(path.join(PWD + '/src/sass/style.scss'), '@charset "utf-8";');
});


// 编译sass
var sass = require('gulp-sass');// 通过npm下载
gulp.task('sass', function () {
    return gulp
    // 在src/sass目录下匹配所有的.scss文件
        .src('src/sass/**/*.scss')

        // 基于一些配置项 运行sass()命令
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'expanded'
        }).on('error', sass.logError))

        // 输出css
        .pipe(gulp.dest('dist/css'));
});

// 为 CSS 3 增加兼容前缀
var autoprefixer = require('gulp-autoprefixer');
gulp.task('autoprefixer', function () {
    return gulp.src('dist/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['ios >= 5','android >= 4.0', 'not ie <= 8', 'last 1 Chrome versions', 'last 1 Safari versions', 'last 1 Firefox versions'],
            cascade: false // 是否把 CSS 属性向右对齐
        }))
        .pipe(gulp.dest('dist/css'));
});

// 对雪碧图的支持
var spritesmith = require('gulp.spritesmith-multi');
gulp.task('sprite', function () {
    return gulp.src('src/sprite/**/*.png')
        .pipe(spritesmith())
        .on('error', function (err) {
            console.log(err)
        })
        .pipe(gulp.dest('dist/img'))
});

/*
* sprite 任务把生成的雪碧图和 css 文件都输出到同一个文件夹里，不利于构建清晰的项目结构和后期维护。
* 使用 merge 将其分别导出到不同的文件夹中。并且我希望所有的 sprite.scss 可以合并成一个 .scss 文件。
* */
var spritesmith = require('gulp.spritesmith-multi');
var merge       = require('merge-stream');
var concat      = require('gulp-concat');

gulp.task('sprite-improved', function () {
    var spriteData = gulp.src('src/sprite/**/*.png')
        .pipe(spritesmith({
            spritesmith: function (options, sprite) {
                options.cssName = sprite + '.scss';
                options.cssSpritesheetName = sprite;
            }
        }));

    var imgStream = spriteData.img
        .pipe(gulp.dest('dist/img'))

    var cssStream = spriteData.css
        .pipe(concat('sprite.scss'))
        .pipe(gulp.dest('src/sass'))

    // Return a merged stream to handle both `end` events
    return merge(imgStream, cssStream)
});

var browserSync = require('browser-sync').create();
gulp.task('default', function() {
    // 监听重载文件
    var files = [
        'dist/html/**/*.html',
        'dist/css/**/*.css',
        'src/js/**/*.js',
        'src/sprite/**/*.png'
    ];
    browserSync.init(files, {
        server: {
            baseDir: "./",
            directory: true
        },
        open: 'external',
        startPath: "dist/html/"
    });

    // 监听编译文件
    gulp.watch("dist/html/**/*.html").on('change', browserSync.reload);
    gulp.watch("src/sass/**/*.scss", ['sass']);
    gulp.watch("src/sprite/**/*.png", ['sprite-improved']);
    gulp.watch("dist/css/**/*.css", ['autoprefixer']);
});