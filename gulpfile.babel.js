/**
 * gulpfile.js 重命名为 gulpfile.babel.js，这样 gulp 执行前会自动调用 babel（gulp 需要，NodeJS 已原生支持） 转换文件。
* */

'use strict';

// import 'babel-polyfill';
import path from 'path'; // node自带组件
import fs from 'fs';
import Q from 'q';

import gulp from 'gulp';
import childProcess from 'child_process';
import sourcemaps from 'gulp-sourcemaps';
import plumber from 'gulp-plumber';
import fse from 'fs-extra'; // 通过npm下载
import sass from 'gulp-sass';
import autoprefixer from 'autoprefixer';
import eventStream from 'event-stream';
import concat from 'gulp-concat';
import concatDir from 'gulp-concat-dir';
import dirConcat from 'gulp-dir-concat';
import concatUtil from 'gulp-concat-util';
import browserSync from 'browser-sync';
import browserify from 'browserify';
import babelify from 'babelify';
import vinylStream from 'vinyl-source-stream';
import vinylBuffer from 'vinyl-buffer';
import uglify from 'gulp-uglify';
import gzip from 'gulp-gzip';
import glob from 'glob';
import watchify from 'watchify';
import rename from 'gulp-rename';
import assign from 'lodash/assign';
import cssnano from 'gulp-cssnano';
import postcss from 'gulp-postcss';
import cssgrace from 'cssgrace';
import cssnext from 'postcss-cssnext';
import template from 'gulp-template';
import jst from 'gulp-jst';
import replace from 'gulp-replace';
import wrap from 'gulp-wrap';
import declare from 'gulp-declare';
import clean from 'gulp-clean';
import handlebars from 'gulp-handlebars';
import del from 'del';
import through2 from 'through2';
import htmlhint from 'gulp-htmlhint';
import htmlmin from 'gulp-htmlmin';
import gutil from 'gulp-util';
import connectLogger from 'connect-logger';
import connectHistoryApiFallback from 'connect-history-api-fallback';
import notify from 'gulp-notify';
import fileInclude from 'gulp-file-include';
import stripDebug from 'gulp-strip-debug';
// 用于获取启动参数，针对不同参数，切换任务执行过程时需要
import {argv} from 'yargs';
import gulpif from 'gulp-if';
import cached from 'gulp-cached';
/*
* Useful when you need to use the file paths from a gulp pipeline in Promise-returning node module.
* Simply pass a promise-returning function such as del and vinyl-paths will provide
* each path in the stream as the first argument.
*/
import vinylPaths from 'vinyl-paths';
import eslint from 'gulp-eslint';
import vueify from 'vueify';

import handleErrors from './src/js/util/handleErrors';
import bundleLogger from './src/js/util/bundleLogger';

// import config from './config.json';

/*babel相关插件
"babel-plugin-external-helpers": "^6.4.0",
"babel-plugin-transform-es2015-classes": "^6.5.2",
"babel-plugin-transform-es2015-modules-commonjs": "^6.5.2",
"babel-plugin-transform-object-assign": "^6.3.13",
 */

gulp.task('notify', function() {
    return gulp.src("./src/js/util/env.js")
        .pipe(notify("Found file: <%= file.relative %>!"));
});


/**
 * @method create: Create a Browsersync instance. you get a unique reference & allows you
 * to create multiple servers or proxies. (browsersync 2 recommend)
 * @param {string} Name: an identifier that can used for retrieval later
 * */
const bs = browserSync.create("myServer");

/**
 * 清除 dist 目录内容
 * */
gulp.task('clean:dist', function() {
    return gulp.src('dist/*') // 此处必须使用 "*"，"**"会导致删除整个 dist 目录
        .pipe(clean());
});

/**
 * 清除 dist 目录内容
 * */
gulp.task('del:dist', function(cb) {
    return del([
        'dist/*'
    ], cb);
});

gulp.task('log', function () {
    return gulp.src('src/lib/**/*.min.+(js|css)')
        .pipe(vinylPaths(function (paths) {
            console.log('Paths:', paths);
            return Promise.resolve();
        }));
});

/**
 * 初始化目录结构
 * */
gulp.task('init', function(cb) {
    // 获取当前工作目录（即执行 node 命令的目录）。PWD：Print Working Directory；CWD：Current Working Directory
    const PWD = process.env.PWD || process.cwd(); // 兼容windows

    const dirs = ['dist','dist/html','dist/css','dist/img','dist/js','dist/lib',
        'src','src/html','src/html/snippet','src/sass','src/js','src/img','src/lib',
        'src/js/app','src/js/common','src/js/util'];

    const files = ['config.json', 'src/html/index.html', 'src/sass/global.scss'];

    const configTemplate =
`{
    "browserify": {
        "debug": true,
        "extensions": [".coffee", ".hbs"],
        "bundleConfigs": [
            {
                "entries":    "src/js/app/freehand/app.js",
                "dest":       "dist/js/app/freehand",
                "outputName": "freehand.min.js"
            }
        ]
    }
}`;

    const htmlTemplate =
`<!DOCTYPE html>
<html lang="zh-CN">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"/>
        <title></title>
        <meta name="apple-touch-fullscreen" content="yes" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    </head>
    <body></body>
</html>`;

    const scssTemplate =
`@charset "utf-8";
body {
    background: #eee;
}

.flex {
    display: flex;
}`;

    // 初始化目录
    dirs.forEach(function (item,index) {
        try { // fs.exists 已被废弃，判断文件是否存在用 fs.stat/statSync，如不存在，回抛异常，必须捕获
            let stat = fs.statSync(path.join(PWD + '/' + item));
            if (stat && stat.isDirectory()) {
                console.error("该目录已存在：", PWD + '/' + item);
            }
        } catch (err) {
            console.error(err.message);
            fse.mkdirSync(path.join(PWD + '/' + item));
        }
    });

    // 初始化文件
    files.forEach(function (item, index) {
        try {
            let stat = fs.statSync(path.join(PWD + '/' + item));
            if (stat && stat.isFile()) {
                console.error("该文件已存在：", PWD + '/' + item);
            }
        } catch (err) {
            console.error(err.message);

            let template = '';

            if (item === 'config.json') {
                template = configTemplate;
            } else if (item === 'src/html/index.html') {
                template = htmlTemplate;
            } else if (item === 'src/sass/global.scss') {
                template = scssTemplate;
            }

            fse.writeFileSync(path.join(PWD + '/' + item), template)
        }
    });

    cb(); // 该任务所有代码都是同步，此处无需显式调用 cb()，cb 一般是在异步方法的回调函数中调用
});

/**
 * 打包 HTML
 * */
gulp.task('html', function() {
    let options = JSON.parse(fs.readFileSync('./.htmlminrc'));

    return gulp.src('src/html/*.html')
        .pipe(plumber()) // htmlhint 允许未闭合标签，htmlmin 压缩时可能报错，必须捕获错误
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(htmlhint('.htmlhintrc'))
        .pipe(htmlhint.reporter("htmlhint-stylish")) // 指定报告样式
        // .pipe(htmlhint.failReporter({ suppress: true })) // 验证失败强制退出
        .pipe(htmlmin(options))
        // .on('error', gutil.log)
        // .on('error', handleErrors) // 如何打印出当前文件全路径
        .pipe(gulp.dest('dist/html'))
        .pipe(bs.stream());
});

// 编译 sass 文件、并自动注入到浏览器
gulp.task('css', function () {
    let processors = [
        // autoprefixer({ // 此时必须使用 autoprefixer ，使用 gulp-autoprefixer 会报错。cssnext 已包含 autoprefixer，故此处隐掉
        //     browsers: ['ios >= 5','android >= 4.0', 'not ie <= 8', 'last 1 Chrome versions', 'last 1 Safari versions', 'last 1 Firefox versions'],
        //     cascade: false // 是否把 CSS 属性向右对齐
        // }),
        cssgrace,
        cssnext
    ];

    return gulp.src(['src/sass/**/app.scss']) // 在src/sass目录下匹配所有的.scss文件
        .pipe(plumber()) //替换错误的pipe方法，使数据流正常运行
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(sourcemaps.init()) //压缩环境出现错误能找到未压缩的错误来源
        .pipe(sass.sync({
            errLogToConsole: true,
            outputStyle: 'compressed', // nested：嵌套缩进的css代码，它是默认值。compact：简洁格式的css代码。expanded：没有缩进的、扩展的css代码。compressed：压缩后的css代码。
            precision: 10, //保留小数点后几位
            sourcemap: true,
            includePaths: ['.']
        }).on('error', sass.logError))
        .pipe(postcss(processors))
        // .pipe(autoprefixer({ // 放在此处时必须使用 gulp-autoprefixer 插件
        //     browsers: ['ios >= 5','android >= 4.0', 'not ie <= 8', 'last 1 Chrome versions', 'last 1 Safari versions', 'last 1 Firefox versions'],
        //     cascade: false // 是否把 CSS 属性向右对齐
        // }))
        // .pipe(cssnano()) // cssnano 必须放到 sourcemaps 前面，否则是对 sourcemaps 的输出压缩。sass 已包含压缩处理，此处无需
        .pipe(sourcemaps.write('.')) // map文件命名
        .pipe(gulp.dest('dist/css')) // 输出 .css 和 .css.map
        .pipe(bs.stream()); // 任务中推荐直接把结果流传给 browsersync 的留
/**
 * The reload method will inform all browsers about changed files
 * and will either cause the browser to refresh, or inject the files where possible.
 * 被 watch 的文件（gulp.watch() or bs.watch()），推荐使用 reload() 触发更新。
 * */
        // .pipe(bs.reload({stream: true}));
});

/**
 * 复制项目需要的第三方工具包
 * */
gulp.task('lib', /*['watchLib'],*/ function() { // 为何此处如果依赖 task：watchLib，只能监测到第一个文件的变动？
    /*
    * glob 默认的 base 路径为开始匹配前的部分，即 *／** 前的部分。如果不修改，此处最终生成的路径为 dist/jquery/...
    *
    * 1. gulp.src(globs[, options])，读取符合条件的文件，然后返回Vinyl files类型的流用于gulp插件进行进一步处理。
    * Vinyl就是一种虚拟文件格式，它能很方便的匹配文件然后读取出来（以stream或者buffer的格式），为每个文件标注path，content，
    * 有利于gulp向文件流pipe plugin
    * options.buffer：是否返回buffer，默认是true。
    * options.read：设置为false，返回的file.content为null，就是不会读取该文件。默认为true。
    * options.base：指glob开始匹配的前面的任何东西，听起来有点拗口。来点例子：
    *
    * 2. gulp.dest(path[, options])：可以被pipe，而且会写入文件，当然，是可以写入多个文件的，当dest的路径不存在时会自动创建。
    * 而这里的path是和file base相关的，就是上面讲到的option.base，所以实际文件的路径由path+base+global.start结合而成，然后再计算出绝对地址。
    * options.cwd：输出文件夹的cwd，默认为：process.cwd()。
    * options.mode：创建文件夹时赋予的权限，默认为：0777。
    *
    * */
    return gulp.src("src/lib/**/*.min.+(js|css)", {base: "src"})
    /*
        * 通过 glob 读取的文件，会给每一个文件依次执行所有 pipe 中的操作函数，完毕后再给下一个文件依次执行。
        * 故不能在单个操作函数里调用 cb()，如有需要，可以写个计数器，参考 task 'browserfiy'
        * you can use objectMode:true if you are processing non-binary streams。
        * through2.obj(fn) is a convenience wrapper around through2({ objectMode: true }, fn)
        * 即如果处理的是 stream，因 stream 中传输的实际上是 buffer（node 中单个 buffer 64k，即长度为 64k 的字节数组）
        * 故 through2 的回调函数中获取的是 chunk（长度 64k 的字节数组）。此处因 gulp.src 默认返回的是 vinylBuffer，
        * 每个都包含单个文件的所有数据，故 through2.obj 回调函数中获取的是单个 file
        * */
        .pipe(through2.obj(function (file, enc, cb) {
            gutil.log(gutil.colors.green(file.path), gutil.colors.magenta("in pipe one"));
            // make sure the file goes through the next gulp plugin。through2.obj 会创建一个每个文件都通过的通道，用完还歹还回去，否则任务终止
            this.push(file);
            // tell the stream engine that we are done with this file
            cb();
            // 必须给回调（此处为through2 的回调，非 gulp）赋值：null，file，否则任务到此终止，后续的 pipe 不会被执行。推荐使用上两行的做法
            // return cb(null, file);
        }))
        .pipe(through2.obj(function (file, enc, cb) {
            gutil.log(gutil.colors.green(file.path), gutil.colors.magenta("in pipe two"));
            this.push(file);
            cb();
        }))
        .pipe(through2.obj(function (file, enc, cb) {
            gutil.log(gutil.colors.green(file.path), gutil.colors.magenta("in pipe three"));
            this.push(file);
            cb();
        }))
        .pipe(gulp.dest("dist"));
        // .on('data', function(event) {
        //     console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        // });
});

gulp.task("watchLib",  function() { // 当多次执行该任务后，有可能无法监听文件变动，重启终端无效，重启 webstorm 即可，Why？
    /*
    *  如果 task A 依赖 task B，B 必须调用 callback，或者返回一个 promise 或 stream。
    *  注：任务中的同步代码是在主线程中执行，pipe 中的函数都是异步执行，在辅助线程
    * */
    let deferred = Q.defer();

    // simulate doing async stuff
    setTimeout(function() {
        /*
         * gulp.watch(glob[, opts], tasks/fn)：watch文件的变化，然后做一定的事情，可以是task或者是一个callback。
         * callback里会带上event对象。包含：
         * event.type：change的类型，有added、deleted、changed。
         * event.path：发生改变的文件的路径。
         * */
        let watcher = gulp.watch("dist/lib/**/*.min.+(js|css)", function(event) {
            gutil.log('File ', gutil.colors.green(event.path), ' was ', gutil.colors.magenta(event.type), ', running tasks...');
        });

        // watcher.on('change', function (event) {
        //     console.log('Event type: ' + event.type);
        // });

        deferred.resolve();
    }, 1000);

    return deferred.promise;
});

/**
 * gulp-template 预编译完是一个匿名函数，需要传给一个变量，更适合在程序里用
 * */
gulp.task('template', function() {
    return gulp.src('src/js/**/*.html')
        .pipe(plumber())
        .pipe(template.precompile())
        .pipe(replace('with', 'if'))
        .pipe(wrap('module.exports = <%= contents %>;'))
        .pipe(rename({
            extname: '.template.js'
        }))
        .pipe(gulp.dest('src/js'))
});

/**
 * js 代码检测
 * */
gulp.task('eslint', function () {
    return gulp.src(['src/js/**/*.js', '!src/js/**/*.template.js'])
        .pipe(cached('eslint'))
        .pipe(eslint())
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

/**
 * 打包 js
 * */
gulp.task('browserify', ['template'], function(cb) {
    const config = JSON.parse(fs.readFileSync('./config.json'));
    let bundleQueue = config.browserify.bundleConfigs.length;

    // Adding debug true for source-map generation. 不加也起作用，是否是因为 customOpts 中配置了 debug: true？
    watchify.args.debug = true;

    let browserifyThis = function(configItem) {
        let customOpts = {
            entries: [configItem.entries],
            debug: true
        };
        // watchify.args：cache: {}, packageCache: {}
        let opts = assign({}, watchify.args, customOpts);

        /**
         * 手工控制 sourceMap 的路径
         * babelify.configure({
         *     sourceMapRelative: 'app/js'
         * })
         * */

        let bundler = browserify(opts);
        bundler = bundler.transform(babelify);

        if (configItem.entries.indexOf('src/js/app/vue') == 0) {
            // 使用单文件组件，打包时必须通过 vueify 转换，且必须安装 vue，否则打包单文件组件会报错：
            // Cannot find module 'vue' from '/Users/songshuohua/WebstormProjects/scaffold/src/js/app/vue/component'
            // 此处没有显示调用 sass 编译，为何单页面组件中 <style> 标签会自动编译，难道 vueify 会自动调用本地安装的 sass？或者因为页面
            // 引入了 vue.js（standalone 方式，带有模版编译器）？
            bundler = bundler.transform(vueify);
        }

        let bundle = function() {
            // Log when bundling starts
            bundleLogger.start(configItem.outputName);

            let reportFinished = function() {
                // Log when bundling completes
                bundleLogger.end(configItem.outputName);

                if(bundleQueue) {
                    bundleQueue--;
                    if(bundleQueue === 0) {
                        // If queue is empty, tell gulp the task is complete.
                        // https://github.com/gulpjs/gulp/blob/master/docs/API.md#accept-a-callback
                        // if parameter of cb() "err" is not null and not undefined, the run will stop, and note that it failed
                        cb();
                    }
                }
            };

            return bundler
                .bundle()
                .on('error', handleErrors) // 此处必须使用 handleErrors，否则出错导致任务中断，且无提示信息
                .pipe(vinylStream(configItem.outputName))
                .pipe(vinylBuffer())
                .pipe(sourcemaps.init({loadMaps: true}))
                .pipe(gulpif(argv.prod, stripDebug())) // 任务带有 dev 参数时，执行 stripDebug 任务
                // 在这里将转换任务加入管道
                .pipe(uglify())
                .on('error', gutil.log)
                .pipe(sourcemaps.write('./'))

                .pipe(gulp.dest(configItem.dest))
                .on('end', reportFinished)
                // Provide `once: true` to restrict reloading to once per stream
                .pipe(bs.stream({once: true})); // 此处必须加 bs.stream
        };

        // Wrap with watchify and rebundle on changes
        bundler = watchify(bundler);
        // Rebundle on update
        bundler.on('update', bundle);

        return bundle();
    };

    // Start bundling with Browserify for each configItem specified
    config.browserify.bundleConfigs.forEach(browserifyThis);
});

/**
 * 预编译模板
 * */
gulp.task('handlebars', function() {
    return gulp.src('src/js/**/component/*.html') // 最终生成的目录，不会包含 ** 之前的部分，故 gulp.dest 中需指定 'dist/js'
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            // namespace: '',
            noRedeclare: true, // Avoid duplicate declarations
            processName: declare.processNameByPath // Include the path as part of the sub-namespace
        }))
        .pipe(dirConcat({ // 如果使用 concatDir，最终是按最上层目录合并，生成 dist/app.js，不知是否和 gulp.src 中路径包含了 "**"有关
            concatName: "template.js",
            // fileNameHandler: function(dirObj) { // 动态生成文件路径
            //     console.log("111",dirObj.cwd);
            //     console.log("222",dirObj.base); // 此处获取的 base 是到 "**" 之前的路径
            //     return dirObj; // 此处必须 返回 dirObj，否则 gulp-dir-concat 报错
            // }
        }))
        .pipe(rename(function(pathObj){
            pathObj.dirname = path.join(pathObj.dirname, "..");
        }))
        .pipe(gulp.dest('dist/js'));
});

/**
 * 建立静态服务器
 * */
gulp.task('server', ['html', 'css', 'lib', 'browserify'], function() {
    // 静态服务器
    bs.init({
        /**
         * Browsersync can watch your files as you work. Changes you make will either be injected
         * into the page(CSS & images) or will cause all browsers to do a full-page refresh.
         * */
        files: [],
        server: {
            baseDir: ["./"], // Serve files from the "current" directory
            directory: true, // Serve files from the "current" directory with directory listing
            index: "index.html", // with a specific index filename
            routes: { // The value is which folder to serve (relative to your current working directory)
                // "/": "./dist/html/"
            }
        },
        /**
         * Decide which URL to open automatically when Browsersync starts. Defaults to "local" if none set.
         * 可选项：
         * false：Stop the browser from automatically opening
         * "local"：Open the localhost URL
         * "external"：Open the external URL - must be online.
         * "ui"：Open the UI - since 2.0.0
         * "tunnel"：Open the tunnel URL - must also set the `tunnel` option
         * */
        open: 'local',
        browser: ["google chrome"/*, "firefox"*/], // Open the site in Chrome & Firefox
        cors: true, // Add HTTP access control (CORS) headers to assets served by Browsersync. Default: false
        notify: false, // Don't show any notifications in the browser. Default: true
        reloadDelay: 0, // Time, in milliseconds, to wait before instructing the browser to reload/inject following a file change event
        reloadDebounce: 0, // Wait for a specified window of event-silence before sending any reload events.
        injectChanges: true, // Try to inject, not do a page refresh. Default: true
        startPath: "/dist/html/index.html", // Open the first browser window at URL + "/info.php"
        minify: false, // Whether to minify client script. Default: true
        host: "127.0.0.1", // Override host detection if you know the correct IP to use
        codeSync: true, // Don't send any file-change events to browsers. Default: true
        timestamps: true, // Don't append timestamps to injected files. Default: true
        ui: {
            port: 3001 // The UI allows to controls all devices, push sync updates and much more.
        },
        port: 3000, // Use a specific port (instead of the one auto-detected by Browsersync)
        middleware: [ // Per-route + global middleware
            // connectLogger(),
            connectHistoryApiFallback(),
            {
                route: "/api", // per-route
                handle: function (req, res, next) {
                    // handle any requests at /api
                }
            }
        ]
    });

    gulp.watch("src/html/**/*.html", ['html']);
    gulp.watch("src/sass/**/*.scss", ['css']);
    gulp.watch("src/js/**/*.html", ['template']);
    gulp.watch("src/lib/**/*.min.+(js|css)", ['lib']);

});





/*
* NodeJS 子进程提供了与系统交互的重要接口，其主要API有： 标准输入、标准输出及标准错误输出的接口。
* child_process即子进程可以创建一个系统子进程并执行shell命令，在与系统层面的交互上挺有用处
* */
gulp.task('ipconfig', function(cb) {
    var exec = childProcess.exec;
    var result = exec('ipconfig', function(err) {
        if (err) {
            console.log('回调执行出错');
            return cb(err); // 返回 error
        } else {
            console.log('回调执行结束');
            cb(); // 完成 task
        }
    });

    result.stdout.on('data', function (data) {
        console.log('标准输出：' + data);
    });

    result.on('exit', function (code) {
        console.log('子进程已关闭，代码：' + code);
    });
});

// 为 CSS 3 增加兼容前缀
gulp.task('autoprefixer', function() {
    return gulp.src('dist/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['ios >= 5','android >= 4.0', 'not ie <= 8', 'last 1 Chrome versions', 'last 1 Safari versions', 'last 1 Firefox versions'],
            cascade: false // 是否把 CSS 属性向右对齐
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('images',function() {
    return gulp.src('app/images/**/*')
        .pipe (
            cache(
                imagemin ({ //使用cache只压缩改变的图片
                    optimizationLevel: 3,         //压缩级别
                    progressive: true,
                    interlaced: true})
                )
            )
        .pipe (gulp.dest ('dist/images'));
});

/*
* Gulp 管道中的「流」不是操作 Strings 和 Buffers 的常规 Node.js 流，而是启用 object mode 的流。Gulp 中的流发送的是 vinyl 文件对象。
* 要在 gulp 及其插件中使用常规读取流，需要把读取流转换为 vinyl 文件对象先。vinyl-source-stream 便是一个转换工具。
* */
gulp.task('browserify-single',function() {
    return browserify('src/js/app1.js')
        .transform(babelify)
        .bundle()
        // browserify 不是一个专门为 gulp 写的包，所有它输出的数据流并不能直接 pipe 给 gulp 使用，使用指定的文件名创建了一个 vinyl 文件对象实例，因此可以不再使用 gulp-rename
        .pipe(vinylStream("app1.bundle.min.js")) // node 常规流转换成 streaming vinyl
        .pipe(vinylBuffer()) // 把 streaming vinyl 转换为 buffered 对象，否则 uglify 报错：Streaming not supported
        .pipe(uglify())
        .pipe(gulp.dest("dist/js/"));
});

/*
 * 多入口文件，错误未解决
 * */
gulp.task('browserify-multi', function(cb) {
    glob('src/js/**/app.js', function(err, files) {
        if(err) {
            cb(err);
        }

        let tasks = files.map(function(entry) {
            return browserify({
                entries: [entry]
            })
                .transform(babelify)
                .bundle()
                .pipe(vinylStream(path.basename(entry)))
                .pipe(rename({
                    extname: '.single-output.bundle.js'
                }))
                .pipe(vinylBuffer())
                .pipe(sourcemaps.init())
                .pipe(uglify())
                .pipe(sourcemaps.write('./'))
                .pipe(gulp.dest("dist/js/"));
        });

        eventStream.merge(tasks).on('end', cb);
    });
});

/*
 * 对于 NodeJS 常规读取流，发射的是数据块（chunk）,默认每个快最大 64 K。
 * 下面代码输出：
 * Read 65536 bytes of data
 * Read 65536 bytes of data
 * Read 21643 bytes of data
* */
gulp.task('node-stream', function(cb) {
    fs.createReadStream('src/lib/react/15.4.1/react.js').on('data', function (chunk) {
        console.log('Read %d bytes of data', chunk.length);
    });

    cb();
});

/*
 * 与常规流不同，gulp.src() 传输的流是把内容转换成 buffer 的 vinyl 文件对象，因此 gulp 中得到的不是数据块（chunk），而是包含 buffer 的虚拟文件。
 * Vinyl 文件对象的内容可以是以下三种形式：Stream，Buffer，null。Gulp 默认使用 Buffer。Vinyl 文件格式有一个 contents 属性来存放文件内容。
 * 虽然一般推荐 Stream 化数据，但是许多 gulp 插件基于使用 Buffer 的库开发。有时则是需要基于完整的源内容做转换，比如基于字符串的正则替换，
 * 分块的文件可能会出现匹配遗漏的情况。同样地，诸如 UglifyJS 及 Traceur compiler 之类的工具也需要完整的文件输入（至少是 JavaScript 句法完整的字符串）
 * 下面的代码展示了整个文件发射给 stream 之前，数据先被转换成 buffer，存放在 vinyl 文件对象中，即回调函数的 file 变量。
 * 以下代码输出：
 * gulp 默认使用 Buffer？ true
 * gulp 默认使用 Stream？ false
 * Read 152715 bytes of data
* */
gulp.task('gulp-buffer', function() {
    return gulp.src('src/lib/react/15.4.1/react.js').on('data', function(file) {
        console.log('gulp 默认使用 Buffer？', file.isBuffer());
        console.log('gulp 默认使用 Stream？', file.isStream());
        console.log('Read %d bytes of data', file.contents.length);
    });
});

/*
* 转换成 buffer 的缺点是对于大文件处理效率低下，因为文件发射回对象流之前必须完整读取。但对于常见的文本文件，
* 如 JavaScript、CSS、模板等，使用 buffer 只是很小的花销，不会真正损害性能。当然，设置 buffer: false 选项，可以让 gulp 禁用 buffer
* 一旦转换成 stream vinyl，必须使用回调才能回去到数据：stream.on()
* */
gulp.task('gulp-stream', function() {
    return gulp.src('src/lib/react/15.4.1/react.js', {buffer: false}).on('data', function(file) {
        var stream = file.contents;
        stream.on('data', function(chunk) {
            console.log('Read %d bytes of data', chunk.length);
        });
    });
});

/*
 * 压缩文件
 * */
gulp.task('gzip', function() {
    return gulp.src('src/lib/react/15.4.1/react.js')
        .pipe(gzip())
        .pipe(gulp.dest('dist/js/lib/'));
});

/**
 "autoprefixer": "^6.6.1", // 根据设置浏览器版本自动处理浏览器前缀，postCSS 版，必须作为 postCSS 的参数
 "babel-core": "^6.21.0",// Babel 的核心 API ，可以直接调用进行转码，
 "babel-eslint": "^7.1.1", // ESLint using Babel as the parser. ESLint actually supports ES2015/ES2016/ES2017, JSX
 "babel-preset-es2015": "^6.18.0", // ES2015 转码规则
 "babel-preset-react": "^6.16.0", // react 转码规则
 "babel-preset-stage-0": "^6.16.0", // ES7 第 0 阶段（包含1,2,3阶段）语法提案的转码规则
 "babel-register": "^6.18.0", // node 中所有通过 require 引入的模块，先解析成 es5，实时解析，效率较差
 "babelify": "^7.3.0", // Browserify的babel转换器
 "babel-polyfill": "^6.20.0", // Babel默认只转换新的句法（syntax），而不转换新的API，比如Iterator、Generator、Proxy、Reflect、Promise等全局对象，以及定义在全局对象上的方法（如Object.assign）
 "browser-sync": "^2.18.5", // 保持多浏览器、多设备同步
 "browserify": "^13.1.1", // CommonJS 打包工具。可以让我们遵循CommonJS规范书写代码，然后经过包装在浏览器中使用。
 "connect-history-api-fallback": "^1.3.0" // Middleware to proxy requests through a specified index page, useful for Single Page Applications that utilise the HTML5 History API.
 "connect-logger": "^0.0.1", // connect middleware for logging express application.
 "cssgrace": "^3.0.0", 是一个 PostCSS 插件。实现了大部分常用的 IE Hack，获取图片宽高等，position: center 等功能。可以配合 Sass 等预处理工具使用，最重要的是它不改变 CSS 原生的语法s
 "del": "^2.2.2", // 删除文件夹、文件（包括管道中的）
 "eslint": "^3.14.1", // for CLI "eslint --init"，need install global
 "eslint-plugin-react": "^6.9.0", //
 "estraverse": "^4.2.0", // 遍历 js 语法树的节点
 "estraverse-fb": "^1.3.1", // 遍历 facebook react jsx文件的语法树
 "event-stream": "^3.3.4", //
 "fs-extra": "^1.0.0", // node 默认 fs 模块的加强版
 "gulp": "^3.9.1",
 "gulp-autoprefixer": "^3.1.1", // 根据设置浏览器版本自动处理浏览器前缀，gulp 版，必须单独使用，不能作为 postCSS 的插件
 "gulp-cache": "^0.4.5", // 图片快取，只有更改过得图片会进行压缩
 "gulp-cached": "^1.1.1",, // keeps an in-memory cache of files (and their contents) that have passed through it. If a file has already passed through and not changed from  the last run it will not be passed downstream
 "gulp-cheerio": "^0.6.2", // Manipulate HTML and XML files with jQuery style in Gulp.
 "gulp-clean": "^0.3.2", // 清空文件夹
 "gulp-clone": "^1.0.0", // Clone files in memory in a gulp stream
 "gulp-concat": "^2.6.1", // 合并文件
 "gulp-concat-dir": "^1.0.0", 按目录合并文件，最终是按最上层目录合并，不知是否和 gulp.src 中路径包含了 "**"
 "gulp-cssnano": "^2.1.2", // 压缩 css
 "gulp-declare": "^0.3.0", // Safely declare namespaces and set their properties
 "gulp-dir-concat": "^0.0.1", // 按目录合并文件
 "gulp-dom-src": "^0.2.0", // Create a gulp stream from script, link, or any set of tags in an HTML file.
 "gulp-eslint": "^3.0.1", //
 "gulp-file": "^0.3.0", // Create vinyl files from a string or buffer and insert into the Gulp pipeline.
 "gulp-file-include": "^1.0.0", // 提供了一个include的方法让我们可以想后端模版一样把公共的部分分离出去
 "gulp-filter": "^5.0.0", // Filter files in a vinyl stream
 "gulp-flatten": "^0.3.1", // remove or replace relative paths for files
 "gulp-gzip": "^1.4.0", // 通过 gulp.dest() 写入文件后，还可以继续传送 stream 给 gzip，压缩后再写入新的文件
 "gulp-handlebars": "^4.0.0", // 预编译模板
 "gulp-htmlhint": "^0.3.1", // 检验 HTML
 "gulp-htmlmin": "^3.0.0", // 压缩 HTML
 "gulp-iconfont": "^8.0.1", // Create icon fonts from several SVG icons
 "gulp-if": "^2.0.2", // 条件成立时，通过管道输送数据到 streamA，反之 streamB
 "gulp-imagemin": "^3.1.1", //  压缩图片
 "gulp-jasmine": "^2.4.2", // 单元测试
 "gulp-jshint": "^2.0.4", // JS 验证工具
 "gulp-jst": "^0.1.1", // 基于 underscore 的模板预编译工具
 "gulp-markdown": "^1.2.0", // 生成 HTML 文档
 "gulp-notify": "^2.2.0", // 显示报错信息（ based on Vinyl Files or Errors ）和报错后不终止当前gulp任务。Send messages to Mac Notification Center, Linux notifications (using notify-send) or Windows >= 8 (using native toaster) or Growl（for Mac） as fallback
 "gulp-order": "^1.1.1", // reorder a stream of files via globs.
 "gulp-plumber": "^1.1.0", // 管道工 不会让错误爆出来 继续执行
 "gulp-postcss": "^6.2.0", // css 后处理工具
 "gulp-rename": "^1.2.2", // 文件重命名工具
 "gulp-replace": "^0.5.4", //  文本替换工具
 "gulp-responsive": "^2.7.0", // generates images at different sizes
 "gulp-rev-append": "^0.1.7", // html引用添加版本号
 "gulp-sass": "^3.0.0", // css 预编译工具。必须用 npm 安装，误用 cnpm，因其安装的是与编译版本 node-sass
 "gulp-sequence": "^0.4.6", // 确保任务结束次序
 "gulp-size": "^2.1.0", // 统计管道里面内容的大小的,上面是用它来显示出压缩前后的大小用来对比用
 "gulp-sourcemaps": "^1.9.1", // 当压缩的JS出错，能根据这个找到未压缩代码的位置 不会一片混乱代码
 "gulp-streamify": "^1.0.2", // 把 vinyl buffer 转换为 vinyl stream
 "gulp-strip-debug": "^1.1.0", // Strip console, alert and debugger statement in Javascript
 "gulp-template": "^4.0.0", // 渲染/预编译 Lo-Dash/Underscore 模板
 "gulp-uglify": "^2.0.0", // 压缩 js
 "gulp-useref": "^3.1.2", // 将html引用顺序的CSS JS 变成一个文件
 "gulp-util": "^3.0.8", // 各种针对 gulp 插件开发的组件集合
 "gulp-wrap": "^0.13.0", // 可以帮助我们定义使用公共的头部和底部
 "htmlhint-stylish": "^1.0.3", // A Stylish reporter for HTMLHint。Uses jshint-stylish to do the actual reporting:
 "jsdom": "^9.9.1", // DOM 解析工具
 "karma": "^1.4.0", // A simple tool that allows you to execute JavaScript code in multiple real browsers.
 "lodash": "^4.17.4", //
 "merge-stream": "^1.0.1", // 合并多个 stream。一个任务中，包含多个 task，此时每个 task 的 stream 都是异步操作，可以通过 merge-stream 合并再返回
 "node-glob": "^1.2.0", // 允许你使用 * 等符号, 来写一个 glob 规则，像在 shell 里一样，获取匹配对应规则的文件。有时我们需要把多个文件添加到 browserify 中，可以借助 node-glob 这个模块实现：
 "postcss-cssnext": "^2.9.0", // 提前使用 css 最新标准，自动转化为兼容版本，已包含 autoprefixer
 "pretty-hrtime": "^1.0.3", // process.hrtime() 返回值转换为单词
 "run-sequence": "^1.2.2", // Run a series of dependent gulp tasks in order
 "through2": "^2.0.3", // Node Stream的简单封装，目的是让链式流操作更加简单
 "vinyl-buffer": "^1.0.0", // 将 vinyl 对象内容中的 Stream 转换为 Buffer。
 "vinyl-paths": "^2.1.0", // get the file path in a vinyl stream
 "vinyl-source-stream": "^1.1.0", // 将 NodeJS 常规流转换为包含 Stream 的 vinyl 对象；
 "vue": "^2.1.10",
 "vue-server-renderer": "^2.1.10",
 "vueify": "^9.4.0", // Browserify transform for Vue.js components, with scoped CSS and component hot-reloading.
 "watchify": "^3.8.0", // 当依赖的文件很多时，browserify 处理速度会很慢，使用 watchify 可以大幅提高处理速度。
 * */

/**
 * package.json 中 scripts 属性如果修改 Start 命令，会导致无法安装新插件： "start": "babel-node --presets es2015 src/js/app1.js",
 * */

/**
 * 手动全局安装：
 * babel-cli（for babel-node 命令）
 * node-gyp
 * node-pre-gyp
 * */

/**
 * Mac 系统注意点：
 * 1. python (v2.7 recommended, v3.x.x is not supported) (already installed on Mac OS X)
 * 2. Xcode
 *   a. You also need to install the Command Line Tools via Xcode. 否则安装 node 插件报错，无法执行 gulp 任务
 *   b. This step will install gcc and the related toolchain containing "make"
 * */